import { useEffect, useState } from "react";
import { io } from "socket.io-client";

function App() {
  const [places, setPlaces] = useState();
  const placesCount = 50;

  const nbPlacesMax = 3;

  const [selectedPlaces, setSelectedPlaces] = useState([]);
  const [nbUsers, setNbUsers] = useState(1);
  const [soldedPlaces, setSoldedPlaces] = useState(null);

  const cancel = () => {
    const cancel = window.confirm("Êtes-vous sûrs de vouloir annuler vos précédentes places ?");

    if (!cancel)
      return false;

    fetch("http://localhost:3003/buy", {
      headers: { "Content-Type": "application/json" },
      method: "DELETE",
      body: JSON.stringify(JSON.parse(localStorage["places"])),
    });

    delete localStorage["places"];
    return true;
  };

  const selectPlace = i => {
    if (selectedPlaces.includes(i) || soldedPlaces?.includes(i))
      return;

    if (localStorage["places"])
      if (!cancel())
        return;

    const sPlaces = [...selectedPlaces];

    if (sPlaces.length >= nbPlacesMax)
      sPlaces.shift();

    sPlaces.push(i);
    setSelectedPlaces(sPlaces);
  };

  const sold = () => {
    if (selectedPlaces.length === 0) {
      window.alert(`Veuillez sélectionner entre 1 et ${nbPlacesMax} place !`);
      return;
    }

    fetch("http://localhost:3003/buy", {
      headers: { "Content-Type": "application/json" },
      method: "POST",
      body: JSON.stringify(selectedPlaces),
    }).then(raw => raw.json())
      .then(json => {
        setSelectedPlaces([]);
        setSoldedPlaces([...json]);
        localStorage["places"] = JSON.stringify(selectedPlaces);
      });
  };

  useEffect(() => {
    const socket = io("http://localhost:3003", {});

    socket.on("co", nb => setNbUsers(nb));

    socket.on("buy", places => setSoldedPlaces(places));

    fetch("http://localhost:3003/buy", {
      headers: { "Content-Type": "application/json" },
      method: "GET",
    }).then(raw => raw.json())
      .then(json => {
        setSoldedPlaces([...json]);
      });

    return () => {
      socket.disconnect();
      console.log("socket disconnected");
    };
  }, []);

  useEffect(() => {
    const a = [];
    let step = 10;
    for (let i = 0; i < placesCount; i++) {
      const y = Math.floor(i / (step)) * 42;
      const x = i % step * 42;

      let bg;

      if (selectedPlaces.includes(i))
        bg = "bg-green-500";
      else if (localStorage["places"] && JSON.parse(localStorage["places"]).includes(i))
        bg = "bg-cyan-400";
      else if (soldedPlaces && soldedPlaces.includes(i))
        bg = "bg-gray-500";

      a.push(<button onClick={() => selectPlace(i)} key={i} style={{ left: x, top: y }} disabled={bg}
                     className={"absolute hover:bg-yellow-300 rounded-sm overflow-hidden flex items-center " +
                       "justify-center border border-gray-300 w-10 h-10 " + bg}>{i}</button>);
    }
    setPlaces(a);
  }, [nbUsers, selectedPlaces, soldedPlaces]);

  return (
    <div>
      <div className="h-screen flex items-center justify-center w-full">
        <img className={"w-40 mr-6"}
             src={"/1200px-Chambéry_Savoie_Mont-Blanc_handball_logo.svg.png"}/>
        <div className={"flex justify-end flex-col"}>

          <p className={"bg-gray-500"}>Places déjà reservées</p>
          {localStorage["places"] !== undefined && <p className={"bg-cyan-400"}>Vos places achetées</p>}
          {selectedPlaces.length > 0 && <p className={"bg-green-500"}>Vos places en cours</p>}
          <p>Nombre d'utilisateurs actuellement : {nbUsers}</p>
          <div className={"w-[420px] h-[210px] relative"}>{places}</div>
          <button onClick={sold}
                  className={"bg-black border-0 mt-1 text-white py-2 hover:bg-green-300 hover:text-black"}>Acheter
          </button>
          {localStorage["places"] !== undefined && <button onClick={cancel}
                                                           className={"bg-black border-0 mt-1 text-white py-2 hover:bg-red-300 hover:text-black"}>Annuler
            la commande
          </button>}
        </div>
      </div>
    </div>
  );
}

export default App;
