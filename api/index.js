const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const port = 3003;

const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const path = require("path");
const fs = require("fs");
const io = new Server(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
    allowedHeaders: ["my-custom-header"],
    credentials: true,
  },
});

io.on("connection", (socket) => {
  nbUsers++;
  io.emit("co", nbUsers);
  console.log("connected", socket.client.id);

  socket.on("disconnect", () => {
    nbUsers--;
    io.emit("co", nbUsers);
    console.log("user disconnected");
  });
});

const filePath = path.resolve(__dirname, "data.json");

const getData = async () => {
  const raw = await fs.promises.readFile(filePath, "utf8");
  return JSON.parse(raw);
};

const saveData = async (data) => {
  await fs.promises.writeFile(filePath, JSON.stringify(data));
};

let nbUsers = 0;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/buy", async (req, res) => {
  let tickets = await getData();
  tickets = [...tickets, ...req.body];
  io.emit("buy", tickets);
  await saveData(tickets)
  res.send(tickets);
});

app.get("/buy", async (req, res) => {
  const tickets = await getData();
  res.send(tickets);
});

app.delete("/buy", async (req, res) => {
  const tickets = await getData();

  for (const nb of req.body) {
    const i = tickets.indexOf(nb);
    tickets.splice(i, 1);
  }

  await saveData(tickets);

  io.emit("buy", tickets);

  res.sendStatus(200);
})

server.listen(port, function () {
  console.log("listening on localhost:" + port);
});